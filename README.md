# gradle-workshop

3 hour workshop about [Gradle](https://gradle.org):
- what is Gradle, why and how to use it (focus on Groovy)
- using Gradle: an existing Gradle project, setup a project from scratch, migrating from Maven
- advanced: multi-project builds, plugins, performance and caching, polyglots

All [the presentation and exercise READMEs](presentation/src/docs/asciidoc/index.adoc) are created with [Asciidoc](https://asciidoctor.org/docs/what-is-asciidoc/) ([try it live](https://asciidoclive.com)).
