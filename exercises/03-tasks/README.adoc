= Gradle Tasks Example

== TaskGraph

See https://tomgregory.com/all-about-the-gradle-task-graph/[] and https://docs.gradle.org/current/javadoc/org/gradle/api/execution/TaskExecutionGraph.html[Gradle API docs for TaskExecutionGraph].

Try:

[source, bash]
....
./gradlew test -q
....

To see the DAG for the test task:

[source, bash]
....
./gradlew tiTree test
....

Examine the task listeners by uncommenting this section:

.build.gradle
[source, groovy]
....
include::build.gradle[tag=task-graph]
....

And then run `./gradlew build -q`

== Getting help for tasks

[source, bash]
....
# describe tasks
./gradlew tasks -q
# even more detail
./gradlew tasks -all -q
./gradlew help --task tiTree
....

Handy plugin:

.build.gradle
[source,groovy]
....
plugins {
include::build.gradle[tags=taskinfo-plugin]
}
....

Using the tasks from the plugin:

[source, bash]
....
./gradlew tiTree build
./gradlew tiOrder build
....


== Tasks

https://docs.gradle.org/current/userguide/more_about_tasks.html[]

=== `dependsOn`

.dependsOn
[source, groovy]
....
project('project-a') {
    tasks.register('taskX')  {
        dependsOn ':project-b:taskY'
        doLast {
            println 'taskX'
        }
    }
}

project('project-b') {
    tasks.register('taskY') {
        doLast {
            println 'taskY'
        }
    }
}
....

=== DefaultTask

Play around with:

* `mustRunAfster <task>`
* `shouldRunAfter <task>`
* `onlyIf {...}`
* `enabled = false`
* `timeout = Duration.ofMillis(10_000)`
* `finalizedBy <task>` - Finalizer tasks will be executed even if the finalized task fails


=== Task Rules

.taskRule example:
[source, groovy]
....
tasks.addRule("Pattern: ping<ID>") { String taskName ->

    if (taskName.startsWith("ping")) {
        task(taskName) {
            doLast {
                println "Pinging: " + (taskName - 'ping')
            }
        }
    }
}

tasks.register('groupPing') {
    dependsOn 'pingServer1', 'pingServer2'
}
....


=== Up-to-date checks

See https://docs.gradle.org/current/userguide/more_about_tasks.html#sec:up_to_date_checks


== Exercises

* Make test3 always run before test1, using `mustRunAfter` (test3 will allway be run when test1 runs)

* Make test3 always run before test1, using `shouldRunAfter` (test3 will allway be run when test1 runs)

* Create the three test tasks dynamically, from within a loop.

* Fail a test and use `./gradlew check -i --continue` to force all other tasks to run after a task fails

* Generate a plantUml diagram illustrating the dependencies of the build task and uncomment the include below.

[plantuml, task-graph]
....
//include::build/task-graph.plantuml[]
....
