package nl.craftsmen.gradle.workshop.greeter

import org.gradle.api.provider.Property

abstract class GreetingPluginExtension {
    abstract Property<String> getMessage()
    abstract Property<String> getGreeter()

    GreetingPluginExtension() {
        message.convention('Hello')
    }
}
