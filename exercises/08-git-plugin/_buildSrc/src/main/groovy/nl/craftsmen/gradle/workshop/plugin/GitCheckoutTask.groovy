package nl.craftsmen.gradle.workshop.docker.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

abstract class GitCheckoutTask extends DefaultTask {

    @Input
    abstract Property<String> getBranch()

    GitCheckoutTask() {

    }

    @TaskAction
    def gitCheckout() {
        new Executor().execute('git','checkout', branch.get())
    }
}
