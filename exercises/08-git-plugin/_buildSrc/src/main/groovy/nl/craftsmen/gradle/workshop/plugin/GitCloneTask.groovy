package nl.craftsmen.gradle.workshop.docker.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import nl.craftsmen.gradle.workshop.process.Executor


abstract class GitCloneTask extends DefaultTask {

    @Input
    abstract Property<String> getRepository()

    GitCloneTask() {

    }

    @TaskAction
    def gitClone(String cloneUrl) {
        new Executor().execute(['git','clone', repository.get()])
    }
}
