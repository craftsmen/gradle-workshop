/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.craftsmen.gradle.workshop.process

import groovy.util.logging.Slf4j

@Slf4j
class Executor implements ExternalProcessExecutor {
    boolean printToConsole

    Executor(boolean printToConsole = true) {
        this.printToConsole = printToConsole
    }

    @Override
    ExternalProcessExecutionResult execute(List<String> command) throws IOException {
        execute(command, null)
    }

    @Override
    ExternalProcessExecutionResult execute(List<String> command, List envp) throws IOException {
        execute(command, envp, new File('.'))
    }

    @Override
    ExternalProcessExecutionResult execute(List<String> command, List envp, File workDir) throws IOException {
        if(OsUtils.isOSWindows()) {
            command.addAll(0, ['cmd', '/c'])
        }
        printCommandLine(command, envp)
        Process process = command.execute(envp, workDir)
        handleProcess(process)
    }

    private void printCommandLine(List<String> command, List envp = null) {
        log.info "Executing external command: '${command.join(' ')}' with environment variables ${envp ?: '[]'}"
    }

    private ExternalProcessExecutionResult handleProcess(Process process) {
        def out = new StringBuilder()
        def err = new StringBuilder()
        process.waitForProcessOutput(out, err)
        if (printToConsole) {
            println (out <<= err)
        }
        new ExternalProcessExecutionResult(exitValue: process.exitValue(), out: out.toString(), err: err.toString())
    }

}
