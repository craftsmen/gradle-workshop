= Gradle Workshop Exercises
:toc: left
:imagesdir: ./build/images
:numbered:
:doctype: book

To build the docs, run:

[source, groovy]
....
./gradlew
....

include::index.adoc[]
