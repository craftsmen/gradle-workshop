= Multi-project Gradle example

.Project structure
[plantuml, project structure]
....
@startuml

package common <<java>>
package api <<java-library>>
package services <<groovy>>

api .up.> common
services .up.> api
services .up.> common

component Slf4j as slf4j
component "commons-lang3" as cl3

api .up.> slf4j : <<api>>
api .up.> cl3 : <<implementation>>

@enduml
....

Run:

[source, bash]
....
# list project structure
./gradlew projects -q

# see the task tree for the :services:test task
./gradlew tiTree :api:check
./gradlew tiOrder :api:check
....

== Dependencies

See: https://docs.gradle.org/current/userguide/core_dependency_management.html

List all project dependencies:

[source, bash]
....
./gradlew -q dependencies
....

To list dependencies for a configuration:

[source, bash]
....
./gradlew -q :services:dependencies --configuration testImplementation
....

Then, to inspec a particular dependency:

[source, bash]
....
./gradlew -q :services:dependencyInsight --dependency org.hamcrest:hamcrest-core:1.3 --configuration testRuntimeClasspath
....

For more on dependency management, see: https://docs.gradle.org/current/userguide/core_dependency_management.html[Dependency management in Gradle]


== Challenges

* Generate the PlantUML diagram like above from the gradle project software model
