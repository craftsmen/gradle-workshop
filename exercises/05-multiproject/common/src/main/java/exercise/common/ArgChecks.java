package exercise.common;

import org.apache.commons.lang3.StringUtils;

public final class ArgChecks {

    public static String notBlank(final String arg, final String argName) throws Exception {
        if (StringUtils.isBlank(arg))
            throw new IllegalArgumentException(String.format("%s may not be empty", argName));
        return arg;
    }

}
