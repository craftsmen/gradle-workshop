package exercise.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import exercise.common.ArgChecks;

public class ArgChecksTest {

    @Test
    public void testArgCheck_notBlank_throws_IAE_for_null() {
        assertThrows(IllegalAccessException.class, () -> {
            String s = ArgChecks.notBlank(null, "argument");
        });
    }

    @Test
    public void testArgCheck_notBlank_throws_IAE_for_empty_string() {
        assertThrows(IllegalAccessException.class, () -> {
            String s = ArgChecks.notBlank("", "argument");
        });
    }

    @Test
    public void testArgCheck_notBlank() throws Exception {
        String s = ArgChecks.notBlank("not a blank string", "argument");
        assertEquals("not a blank string", s);
    }

}
