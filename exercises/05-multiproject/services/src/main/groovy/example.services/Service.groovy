package example.services

import example.api.API
import exercise.common.ArgChecks
import groovy.util.logging.Slf4j

@Slf4j
class Service implements API {

    String sayHello(String name) {
        log.info("saying hello")
        ArgChecks.notBlank(name, 'name')
        "Hello $name"
    }

}
