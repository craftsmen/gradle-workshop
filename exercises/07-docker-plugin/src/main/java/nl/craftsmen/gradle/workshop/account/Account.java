package nl.craftsmen.gradle.workshop.account;

import java.math.BigDecimal;

public class Account {

    private Long id;
    private String owner;
    private BigDecimal balance;

    protected Account() {
        // jackson
    }

    public Account(Long id, String owner, BigDecimal balance) {
        this.id = id;
        this.owner = owner;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }

}
