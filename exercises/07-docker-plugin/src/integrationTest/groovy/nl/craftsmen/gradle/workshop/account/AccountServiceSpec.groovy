package nl.craftsmen.gradle.workshop.account

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification
import spock.lang.Unroll

class AccountServiceSpec extends Specification {

    private static ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    static String SERVICE_URL = "http://localhost:8080/accounts/"

    static class TestClient {
        final String serviceUrl
        TestClient(final String url) {
            serviceUrl = url
        }
        Account getAccount(Long id) {
            def json = "${serviceUrl}?id=$id".toURL().text
            OBJECT_MAPPER.readValue(json, Account)
        }
    }

    @Unroll
    def "returns account owner #owner for account #id"(id, owner) {
        when:
        Account account = new TestClient(SERVICE_URL).getAccount((long)id)

        then:
        account.id == id
        account.owner == owner
        println "Account $id of $owner has balance ${account.balance}"

        where:
        id | owner
        1 | "John Doe"
        2 | "Foo Bar"
    }

}
