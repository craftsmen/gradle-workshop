package nl.craftsmen.gradle.workshop.docker
// naive docker cli wrapper
class StaticDockerContainer {

    static File cidFile = new File(".containerId")

    static boolean isRunning(String containerId) {
        new nl.craftsmen.gradle.workshop.process.Executor().execute(['docker', 'ps', '-aq']).out.contains(containerId)
    }

    static start(String image, int port) {
        if (cidFile.exists()) {
            try {
                if (isRunning(cidFile.text)) {
                    stop(cidFile.text)
                }
            } finally {
                cidFile.delete()
            }
        }
        new nl.craftsmen.gradle.workshop.process.Executor()
                .execute(['docker', 'run',
                        '-p', "$port:$port",
                        '--cidfile', "${cidFile}",
                        '-d', '--rm', image])
    }

    static stop(String containerId) {
        new nl.craftsmen.gradle.workshop.process.Executor().execute(['docker', 'stop', containerId])
    }

    static String getContainerId() {
        cidFile.exists() ? cidFile.text : null
    }

    static String stop() {
        stop(containerId)
    }

}
