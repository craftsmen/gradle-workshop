package nl.craftsmen.gradle.workshop.docker.plugin

import nl.craftsmen.gradle.workshop.docker.tasks.StartContainerTask
import nl.craftsmen.gradle.workshop.docker.tasks.StopContainerTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class DockerContainerPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.tasks.register('startContainer', StartContainerTask) {
            group 'docker'
            image = project.tasks.buildImage.imageId
            port = 8080
            dependsOn project.tasks.buildImage
        }

        project.tasks.register('stopContainer', StopContainerTask) {
            group 'docker'
            dependsOn project.tasks.buildImage, project.tasks.startContainer
        }
    }

}
