= The Gradle DSL
:toc: left

This is an example project for playing around with properties.

== Gradle DSL

=== Properties

[source,groovy]
....
<obj>.<name>                // Get a property value
<obj>.<name> = <value>      // Set a property to a new value
"$<name>"                   // Embed a property value in a string
"${<obj>.<name>}"           // Same as previous (embedded value)

// files:
file("$buildDir/classes")

// output
println "Destination: ${myCopyTask.destinationDir}"
....

NOTE: Single quoted strings in Groovy have no string-interpolation! Use double quoted string literals to expand expressions like `${myProperty}`.

=== Property evaluation
If the name is unqualified, then it may be one of the following:

* A task instance with that name.
* A property on Project.
* An extra property defined elsewhere in the project.
* A property of an implicit object within a block.
* A local variable defined earlier in the build script.

=== Methods

[source,groovy]
....
<obj>.<name>()              // Method call with no arguments
<obj>.<name>(<arg>, <arg>)  // Method call with multiple arguments
<obj>.<name> <arg>, <arg>   // Method call with multiple args (no parentheses)
....

Methods can take a Groovy `Closure` as argument:

[source,groovy]
....
myCopyTask.include '**/*.xml', '**/*.properties'

ext.resourceSpec = copySpec()   // `copySpec()` comes from `Project`

file('src/main/java')
println 'Hello, World!'
....

=== Configuration blocks

Blocks are also methods, just with specific types for the last argument.

[source,groovy]
....
<obj>.<name> {
     ...
}

<obj>.<name>(<arg>, <arg>) {
     ...
}
....

.Example
[source,groovy]
....
plugins {
    id 'java-library'
}

configurations {
    assets
}

sourceSets {
    main {
        java {
            srcDirs = ['src']
        }
    }
}

dependencies {
    implementation project(':util')
}
....

There are two important aspects of blocks that you should understand:

They are implemented as methods with specific signatures.

They can change the target ("delegate") of unqualified methods and properties.

More info: https://docs.gradle.org/current/userguide/groovy_build_script_primer.html#block_method_signatures


=== Local variables

[source, groovy]
....
def <name> = <value>        // Untyped variable
<type> <name> = <value>     // Typed variable
....

.Example
[source, groovy]
....
def i = 1
String errorMsg = 'Failed'
....

=== Extra properties

See https://docs.gradle.org/current/userguide/writing_build_scripts.html#sec:extra_properties

Using extra properties in a project:

.build.gradle
[source, groovy]
....
ext {
    springVersion = "3.1.0.RELEASE"
    notificationEmail = "build@master.org"
}
....

Inspect the properties with:

[source, bash]
....
./gradlew -q printProperties
....


== Playing around

* run `./gradlew` - inspect the result:
** a `.gradle` dir was created
** a `build` dir was created
* Add a group and description to the custom tasks, refresh the project and inspect the Gradle tab.
* Run some tasks
* Break the build by changing something and see what happens...

* run `./gradlew -DmagicNumber=21` with some different numbers; also try ranges, like `1..3,10`

* run `./gradlew -PmagicNumber=5 -PmagicNumberApi=math` and check that the default value was `trivia`, as specified in `gradle.properties`.

=== Change magicNumber in gradle.properties

Change the default magicNumber in `gradle.properties` in the root of this project and run `./gradlew`.

=== Add `magicNumber` to `~/.gradle/gradle.properties`

Add a magicNumber override in `gradle.properties` in the root of your $GRADLE_HOME directory (defaults to `~/.gradle`).

=== More info

https://docs.gradle.org/current/userguide/writing_build_scripts.html

== Configuring Gradle

=== Passing in properties via the command line

Run:
`./gradlew -q -PcommandLineProjectProp=commandLineProjectPropValue -Dorg.gradle.project.systemProjectProp=systemPropertyValue printProps`

=== Gradle Daemon

To see the Gradle daemon status:

[source,bash]
....
./gradlew --status
....


=== Configuration evaluation order (first wins)

** Command-line flags such as `--build-cache`. These have precedence over properties and environment variables.
** System properties such as `systemProp.http.proxyHost=somehost.org` stored in a `gradle.properties` file.
** Gradle properties such as `org.gradle.caching=true` that are typically stored in a `gradle.properties` file in a project root directory or `GRADLE_USER_HOME` environment variable.
** Environment variables such as `GRADLE_OPTS` sourced by the environment that executes Gradle.



== Conclusion

Parameters can be set in `gradle.properties`, and can be overridden in the `$GRADLE_HOME/gradle.properties` file. The command line parameter overrides all default values set in `gradle.properties` files.
