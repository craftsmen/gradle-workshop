== Introducing Gradle
:toc: left
:imagesdir: images
:source-highlighter: highlightjs
:highlightjs-languages: bash, groovy, json
:numbered:


=== What is Gradle?

- Very extensible general-purpose (build) automation tool
- High performance
- Open Source: Apache License 2.0 (Java)
- Declarative
- Based on Maven Conventions
- Excellent IDE integration

[.notes]
--
* Enterprise Edition with extra features:
** Speed: build cache
** Insight: scans for Maven and Gradle
** Productivity: tools, support (share scans)
--


=== Groovy DSL: a build script

// https://docs.gradle.org/current/userguide/groovy_build_script_primer.html#groovy_build_script_primer

[source,groovy]
----
version = '1.0.0'
group = 'nl.craftsmen.gradle'

dependencies {
    // TODO ...
}
----

Both version and dependencies, refer to the Gradle API class https://docs.gradle.org/current/dsl/org.gradle.api.Project.html[org.gradle.api.Project]


=== Gradle compared to Ant

* Ant is _imperative_, Gradle is _declarative_
* Ant has no dependency mgmt (but Ivy)
* Ant has no true conventions
* Ant is old


=== Migrating from Ant

[.halign-left]
--
* Import an Ant build directly into build.gradle using `ant.importBuild()`
--

[.notes]
--
* See https://docs.gradle.org/current/userguide/migrating_from_ant.html
* Please someone explain Ant
* discuss advantages of Ant
* discuss advantages of Gradle vs Ant
* https://docs.gradle.org/current/userguide/migrating_from_ant.html[]
--


=== Gradle compared to Maven

* maven dependency management
* same repositories can be used
* 'tasks' are phase-bound, explicit order
* explicit (still imperative...)
* convention over configuration

[.notes]
--
* Please someone explain Maven
* discuss advantages of Maven
* discuss advantages of Gradle vs Maven
* https://docs.gradle.org/current/userguide/migrating_from_maven.html
--

=== Migrating from Maven

image::gradle-vs-maven-performance.png[]

[.notes]
--
* https://docs.gradle.org/current/userguide/migrating_from_maven.html
--

