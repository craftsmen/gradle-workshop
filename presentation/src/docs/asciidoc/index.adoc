= Gradle Workshop
:toc: left
:sectnums:
:doctype: book
:imagesdir: images

image::gradle.jpg[]

Marwik van Doorn (marwik@craftsmen.nl)

~180 min

`git@gitlab.com:craftsmen/gradle-workshop.git`

// about revealjs: https://zenika.github.io/adoc-presentation-model/reveal/reveal-my-asciidoc.html


include::_01_intro.adoc[]

include::_02_introducing_gradle.adoc[]

include::_03_exercises.adoc[]

include::_04_references.adoc[]


== Discussion / Questions


// revealjs plugins: menu
++++
<script src="bower_components/reveal.js-menu/menu.js"></script>
<script>
    Reveal.initialize({
        plugins: [ RevealMenu ]
    });
</script>
++++
